#!/usr/bin/env python3
import time
import subprocess
import lcd

LCD_PINS = lcd.LCD(47, 46, 66, 67, 69, 68, 45, 44, 23, 26)
PWM_PIN = 'P9_14'


def setup():
    lcd.setup(LCD_PINS)
    lcd.set_brightness(PWM_PIN, 100)
    display_string("Starting...")
    time.sleep(1)


def loop(device):
    setup()
    metadata = {'current': None, 'old': None}
    volume = {'current': None, 'old': None}
    is_playing = None

    while True:
        metadata['old'] = metadata['current']
        metadata['current'] = device.song_metadata
        volume['old'] = volume['current']
        volume['current'] = get_volume()

        if (metadata['current'] and metadata['current'] != metadata['old']):
            display_metadata(metadata['current'])
            is_playing = True
        elif not device.is_playing:
            if is_playing:
                lcd.clear_display(LCD_PINS)
                display_string("Stopped")
            is_playing = False
        elif device.is_playing:
            if not is_playing:
                display_metadata(metadata['current'])
            is_playing = True

        if (volume['current'] and volume['current'] != volume['old']):
            display_line('Volume: ' + volume['current'] + '%', 3)

        time.sleep(0.2)


def get_volume():
    output = subprocess.check_output(['amixer', 'sget', 'Headphone'],
                                     stderr=subprocess.PIPE).decode()
    start = output.find('[') + 1
    end = output.find('%')
    return output[start:end]


def display_metadata(metadata):
    try:
        display_line(metadata['Title'], 0)
        display_line(metadata['Artist'], 1)
        display_line(metadata['Album'], 2)
    except TypeError:
        pass


def display_line(string, line):
    lcd.move_cursor(LCD_PINS, 0, line)
    for i in range(20):  # Lines are 20 chars long.
        if i < len(string):
            char = string[i]
        else:  # Continue writing blanks to clear old data.
            char = ' '
        binary = '{0:b}'.format(ord(char)).zfill(8)
        lcd.send_byte(LCD_PINS, binary, is_character=True)


def display_string(string):
    length = 20
    words = string.split(' ')
    lines = []
    current_line = ''
    for word in words:
        if current_line:
            current_line += ' '
        if len(word) + len(current_line) > length:
            lines.append(current_line)
            current_line = ''
        current_line += word
    if current_line:
        lines.append(current_line)

    lcd.clear_display(LCD_PINS)
    for i, line in enumerate(lines[:4]):
        lcd.move_cursor(LCD_PINS, 0, i)
        for char in line:
            binary = '{0:b}'.format(ord(char)).zfill(8)
            lcd.send_byte(LCD_PINS, binary, is_character=True)
