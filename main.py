#!/usr/bin/env python3
import subprocess
import threading
import time
import bluetooth
import display
import controls


def main():
    device = bluetooth.Device()
    # Start pulseaudio if it is not already running.
    try:
        subprocess.check_call(['pulseaudio', '--check'])
    except subprocess.CalledProcessError:
        subprocess.call(['pulseaudio', '-D'])
        time.sleep(1)

    setup_device(device)
    display_thread = threading.Thread(target=display.loop, args=(device,),
                                      daemon=True)
    display_thread.start()
    controls.setup()

    loop(device)


# FIXME: WHY does this require connecting, disconnecting, and re-connecting?
def setup_device(device):
    device.connect()
    device.disconnect()
    device.connect()
    time.sleep(1)
    device.play()


def loop(device):
    LOOP_TIME = 0.1
    old_button = None
    old_button_duration = 0

    while True:
        new_button = controls.read()
        if old_button:
            if old_button != new_button:
                handle_button(device, old_button, old_button_duration)
                old_button_duration = 0
            else:
                old_button_duration += LOOP_TIME

        old_button = new_button
        time.sleep(LOOP_TIME)


def handle_button(device, button, duration):
    if button == 'volume_up':
        print('vol up')
        set_volume(do_increase=True)
    elif button == 'volume_down':
        print('vol dwn')
        set_volume(do_increase=False)
    elif button == 'forward':
        print('skip')
        device.skip()
    elif button == 'back':
        print('back')
        device.previous()
    elif button == 'mode':
        if duration < 5:
            bluetooth.allow_pairing()
        else:
            device.disconnect()
            subprocess.call(['sudo', 'poweroff', '--reboot'])


def set_volume(do_increase):
    amount = '2+' if do_increase else '2-'
    command = ['amixer', 'sset', 'Headphone', amount]
    subprocess.call(command)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit
