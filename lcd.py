#!/usr/bin/env python3
import collections
import glob
import time

LCD = collections.namedtuple('LCD', ['e', 'rs', 'db7', 'db6', 'db5', 'db4',
                                     'db3', 'db2', 'db1', 'db0'])


def setup(lcd):
    _export_gpio(lcd)
    for _ in range(3):  # Send reset command 3x
        send_byte(lcd, '00111000', False)
    set_display(lcd, display_on=True, underline=False, blink=False)
    clear_display(lcd)
    move_cursor(lcd, 0, 0)


def set_display(lcd, display_on=True, underline=True, blink=True):
    command = list('00001') + ['1' if param else '0' for param in
                               (display_on, underline, blink)]
    send_byte(lcd, command, False)


def set_brightness(pwm_pin, level):
    """ level is a range from 0 (backlight off) to 100 (max brightness). """
    if (level > 100):
        level = 100
    elif (level < 0):
        level = 0

    slots = glob.glob('/sys/devices/bone_capemgr.*/slots')[0]
    try:
        with open(slots, 'w') as f:
            f.write('am33xx_pwm')
    except FileExistsError:
        pass
    try:
        with open(slots, 'w') as f:
            f.write('bone_pwm_' + pwm_pin)
    except FileExistsError:
        pass

    time.sleep(1)
    pwm_directory = glob.glob(
        '/sys/devices/ocp.*/pwm_test_' + pwm_pin + '*')[0]
    with open(pwm_directory + '/period', 'w') as f:
        f.write(str(500000))
    with open(pwm_directory + '/duty', 'w') as f:
        value = int(500000 - (500000 / 100 * level))
        f.write(str(value))


def move_cursor(lcd, x, y):
    line_offsets = [0, 64, 20, 84]
    command = '1' + '{0:b}'.format(line_offsets[y] + x).zfill(7)
    send_byte(lcd, command, False)


def clear_display(lcd):
    send_byte(lcd, '00000001', False)


def send_byte(lcd, command, is_character=False):
    _set_pin(lcd.e, True)
    _set_pin(lcd.rs, is_character)
    for i, bit in enumerate(command):
        _set_pin(lcd[i + 2], bit == '1')
    _set_pin(lcd.e, False)


def _export_gpio(lcd):
    directory = '/sys/class/gpio/'
    export_file = directory + 'export'
    for pin in lcd:
        try:
            with open(export_file, 'w') as f:
                f.write(str(pin))
        except OSError:  # Error thrown if already exported
            pass
        direction_file = directory + 'gpio' + str(pin) + '/direction'
        with open(direction_file, 'w') as f:
            f.write('out')


def _unexport_gpio(lcd):
    filename = '/sys/class/gpio/unexport'
    for pin in lcd:
        with open(filename, 'w') as f:
            f.write(str(pin))


def _set_pin(pin, set_high):
    filename = '/sys/class/gpio/gpio' + str(pin) + '/value'
    with open(filename, 'w') as f:
        f.write('1' if set_high else '0')
