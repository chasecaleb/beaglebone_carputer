#!/usr/bin/env python3


def setup():
    try:
        with open('/sys/devices/bone_capemgr.9/slots', 'a') as f:
            f.write('BB-ADC')
    except FileExistsError:  # ADC already set up.
        pass


def read():
    def poll():
        with open('/sys/bus/iio/devices/iio:device0/in_voltage0_raw') as f:
            mode = int(f.read().strip())
        with open('/sys/bus/iio/devices/iio:device0/in_voltage1_raw') as f:
            others = int(f.read().strip())
        return mode, others

    mode, others = poll()

    if (mode > 2048):
        return 'mode'
    elif (others > 512):
        if (others < 1500):
            return 'volume_down'
        elif (others < 2500):
            return 'volume_up'
        elif (others < 3500):
            return 'back'
        else:
            return 'forward'
    else:
        return None
