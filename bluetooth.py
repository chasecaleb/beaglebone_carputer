#!/usr/bin/env python3
import subprocess
import threading
import queue
import time
import xml.etree.ElementTree as ElementTree
import dbus


class BluezWrapper:
    """ Wraps access to Bluez DBus properties and methods.
    Dynamically create property and method attributes via introspection. Note
    that naming breaks Python style by using CamelCase.

    See https://git.kernel.org/cgit/bluetooth/bluez.git/tree/doc/ for
    documentation of interfaces and their respective properties and functions.
    """

    _bus = dbus.SystemBus()

    def __init__(self, interface, object_path):
        """ Refer to bluez documention for interface and object_path info. """
        self._interface_name = interface
        self._obj = self._bus.get_object('org.bluez', object_path)
        self._properties = dbus.Interface(
            self._obj, 'org.freedesktop.DBus.Properties')
        self._methods = dbus.Interface(self._obj, interface)
        self._introspect()

    def _introspect(self):
        root_raw = dbus.Interface(self._obj, dbus.INTROSPECTABLE_IFACE)
        root = ElementTree.fromstring(root_raw.Introspect())

        interface = None
        for i in root:
            if i.attrib['name'] == self._interface_name:
                interface = i
                break
        else:
            raise AttributeError

        for child in interface:
            if child.tag == 'method':
                func = getattr(self._methods, child.attrib['name'])
                setattr(self, child.attrib['name'], func)
            elif child.tag == 'property':
                self._add_property(child.attrib['name'])

    def _add_property(self, name):
        fget = lambda self: self._properties.Get(self._interface_name, name)
        fset = lambda self, value: self._properties.Set(self._interface_name,
                                                        name, value)
        setattr(self.__class__, name, property(fget, fset))


# TODO: handle these try/excepts in a better way before bad things happen.
class Device:

    def __init__(self, device_name=None):
        """ Connect to last used device if device_name is None. """
        if not device_name:
            # FIXME: track last device
            device_name = 'dev_A0_F4_50_08_ED_89'
        self.device_name = device_name

    @property
    def device(self):
        path = '/org/bluez/hci0/' + self.device_name
        try:
            return BluezWrapper('org.bluez.Device1', path)
        except AttributeError:
            pass

    @property
    def player(self):
        path = '/org/bluez/hci0/' + self.device_name + '/player0'
        try:
            return BluezWrapper('org.bluez.MediaPlayer1', path)
        except AttributeError:
            pass

    @property
    def connected(self):
        try:
            return self.device.Connected
        except dbus.exceptions.DBusException:
            pass

    @property
    def song_metadata(self):
        try:
            return self.player.Track
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass

    @property
    def position(self):
        try:
            return self.player.Position
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass

    @property
    def is_playing(self):
        try:
            return self.player.Status == 'playing'
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass

    def connect(self):
        self.device.Connect()
        # try:
            # self.device.Connect()
        # except dbus.exceptions.DBusException:
            # pass
        # except AttributeError:
            # pass

    def disconnect(self):
        self.device.Disconnect()
        # try:
            # self.device.Disconnect()
        # except dbus.exceptions.DBusException:
            # pass
        # except AttributeError:
            # pass

    def play(self):
        try:
            self.player.Play()
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass

    def skip(self):
        try:
            self.player.Next()
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass

    def previous(self):
        try:
            self.player.Previous()
        except dbus.exceptions.DBusException:
            pass
        except AttributeError:
            pass


def allow_pairing(timeout=30):
    """ Turn adapter discovery on and accept first pairing request.
    WARNING: blocks until device paired or timeout reached.
    ...And seriously bluetoothctl? Why don't you work non-interactively?
    """
    def send_to_queue(out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)

    def write_stdin(line):
        proc.stdin.write(str(line + '\n').encode())
        proc.stdin.flush()

    stdout = queue.Queue()
    proc = subprocess.Popen(['bluetoothctl'], stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE)
    t = threading.Thread(target=send_to_queue, args=(proc.stdout, stdout))
    t.daemon = True
    t.start()

    commands = ['power on', 'agent on', 'default-agent', 'discoverable on']
    for cmd in commands:
        write_stdin(cmd)
        time.sleep(0.2)

    authorization_lines = ['Request confirmation', 'Authorize service']
    start_time = time.time()
    timed_out = False
    for line in authorization_lines:
        while True:
            if time.time() > start_time + timeout:
                timed_out = True
                break
            try:
                if line in stdout.get_nowait().decode():
                    write_stdin('yes')
                    break
            except queue.Empty:
                pass
        if timed_out:
            break
    write_stdin('discoverable off')
