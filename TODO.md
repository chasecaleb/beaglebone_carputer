# TODO

## High priority
* Controls
    * Refactor threading design
    * Use mode steering wheel control to open menu
    * ...Make a menu -- e.q., brightness, pairing, shutdown, etc
* Get EQ working -- reverse engineer qpaeq dbus calls?

## Low priority
* Allow pairing/connecting to other devices (and reconnect to last used)
* Increase LCD write speed -- use direct memory control instead of sysfs
* Run through "real world" startup/play/shutdown a bit more to check for bugs
    * Especially regarding startup bluetooth connection and pulseaudio

## Miscellaneous
* (?) Set safe volume on boot
* Order better RCA cables
